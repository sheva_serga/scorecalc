﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Columns;
using BenchmarkDotNet.Attributes.Exporters;
using BenchmarkDotNet.Attributes.Jobs;
using BenchmarkDotNet.Engines;
using ScoreCalculator.Common.DAL;
using ScoreCalculator.DAL.Services;
using ScoreCalculator.DAL.Services.Score;
using System;

namespace ScoreCalculator.Banchmarks
{
    //Will show valid results when there will be a lot of comments for products 
    [RPlotExporter, RankColumn]
    [SimpleJob(RunStrategy.Monitoring, launchCount: 10, warmupCount: 5, targetCount: 5)]
    public class ScoreServiceBanchmark
    {
        private const String CONNECTION_STRING = @"Server=localhost;Database=AdventureWorks2016;Trusted_Connection=True";

        private Int32[] _data;
        private DbStoreService _storeService;

        [GlobalSetup]
        public void Setup()
        {
            _data = new Int32[] { 709, 798, 937 };
            _storeService = new DbStoreService(CONNECTION_STRING);
        }

        [Benchmark(Description = "AVG logic banchmark")]
        public void AvgServiceBanchmark()
        {
            IDbScoreService service = new DbAvarageScoreService(_storeService);

            service.GetScoreDataAsync(_data).ConfigureAwait(false).GetAwaiter();
        }
            

        [Benchmark(Description = "Prepared logic banchmark")]
        public void PreparedLogicBanchmark()
        {
            IDbScoreService service = new DbPreparedKeywordScoreService(_storeService);

            service.GetScoreDataAsync(_data).ConfigureAwait(false).GetAwaiter();
        }

        [Benchmark(Description = "Active load logic banchmark", Baseline = true)]
        public void ActiveLoadLogicBanchmark()
        {
            IDbScoreService service = new DbKeywordScoreService(_storeService);

            service.GetScoreDataAsync(_data).ConfigureAwait(false).GetAwaiter();
        }
    }
}
