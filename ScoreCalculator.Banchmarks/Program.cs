﻿using BenchmarkDotNet.Running;
using System;

namespace ScoreCalculator.Banchmarks
{
    class Program
    {
        //Please close all programs before run benchmarks
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<ScoreServiceBanchmark>();

            Console.ReadKey();
        }
    }
}
