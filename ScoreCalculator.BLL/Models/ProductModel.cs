﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.BLL.Models
{
    public class ProductModel
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public Decimal ReviewScore { get; set; }

        public ProductModel(Int32 id, String name, Decimal reviewScore)
        {
            Id = id;
            Name = name;
            ReviewScore = reviewScore;
        }
    }
}
