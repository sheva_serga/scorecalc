﻿using ScoreCalculator.BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCalculator.BLL.Services
{
    public interface IProductService
    {
        Task<ProductModel> GetProductAsync(Int32 productId);
        Task<List<ProductModel>> GetProductModelsByPageAsync(Int32 pageNumber, Int32 perPage);
    }
}
