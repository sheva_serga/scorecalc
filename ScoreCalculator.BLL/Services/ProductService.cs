﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ScoreCalculator.BLL.Models;
using ScoreCalculator.Common.DAL;
using ScoreCalculator.Common.Models;

namespace ScoreCalculator.BLL.Services
{
    internal class ProductService : IProductService
    {
        private readonly IDbScoreService _scoreService;
        private readonly IDbProductService _productService;

        public ProductService(IDbProductService productService, IDbScoreService scoreService)
        {
            _scoreService = scoreService;
            _productService = productService;
        }

        async Task<ProductModel> IProductService.GetProductAsync(Int32 productId)
        {
            IProductData product = await _productService.GetProductAsync(productId);

            if (product == null) return null;

            IScoreData score = await _scoreService.GetScoreDataAsync(productId);

            return new ProductModel(
                      product.Id
                    , product.Name
                    , score?.ReviewScore ?? Decimal.Zero
                );
        }

        async Task<List<ProductModel>> IProductService.GetProductModelsByPageAsync(Int32 pageNumber, Int32 perPage)
        { 
            List<ProductModel> productModels = new List<ProductModel>();

            List<IProductData> products = await _productService.GetProductsByPageAsync(pageNumber, perPage);

            if (products == null || products.Count == 0) return productModels;

            Dictionary<Int32, Decimal> scores = (await _scoreService.GetScoreDataAsync(products.Select(p => p.Id)))
                .ToDictionary(k => k.Id, v => v.ReviewScore);

            Decimal revicewScore;

            foreach (IProductData item in products)
            {
                if (!scores.TryGetValue(item.Id, out revicewScore)) revicewScore = Decimal.Zero;

                productModels.Add(new ProductModel(item.Id, item.Name, revicewScore));
            }

            return productModels;
        }
    }
}
