﻿using Microsoft.Extensions.DependencyInjection;
using ScoreCalculator.BLL.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.BLL
{
    public static class BusinessLogicInstallerExtention
    {
        public static IServiceCollection AddBusinessLogicLayer(this IServiceCollection services)
        {
            services.AddSingleton<IProductService, ProductService>();

            return services;
        }
    }
}
