﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ScoreCalculator.Common.DAL;
using ScoreCalculator.DAL;
using System;

namespace ScoreCalculator.Generator
{
    class Program
    {
        //Will run by schedule
        static void Main(string[] args)
        {
            IServiceCollection collection = new ServiceCollection();

            IConfiguration configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            collection.AddOptions();
            collection.AddDalLayer(configuration);

            ServiceProvider provider =  collection.BuildServiceProvider();

            IDbScoreUpdateService productService = provider.GetRequiredService<IDbScoreUpdateService>();

            productService.GenerateProductsRating();

        }
    }
}
