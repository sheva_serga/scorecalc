﻿using ScoreCalculator.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCalculator.Common.DAL
{
    public interface IDbScoreService
    {
        Task<IScoreData> GetScoreDataAsync(Int32 productId);
        Task<List<IScoreData>> GetScoreDataAsync(IEnumerable<Int32> productIds);
    }
}
