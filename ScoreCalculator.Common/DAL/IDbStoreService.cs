﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScoreCalculator.Common.DAL
{
    public interface IDbStoreService
    {
        Task<T> SelectAsync<T>(String query, Object parameters = null);
        Task<List<TResult>> SelectListAsync<T, TResult>(String query, Object parameters = null) where T : TResult;
        Task<Int32> ExecuteAsync(String query, Object parameters = null);
    }
}
