﻿using ScoreCalculator.Common.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScoreCalculator.Common.DAL
{
    public interface IDbProductService
    {
        Task<IProductData> GetProductAsync(Int32 productId);
        Task<List<IProductData>> GetProductsByPageAsync(Int32 pageNumber, Int32 perPage);
    }
}
