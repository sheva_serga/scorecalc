﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.Common.Models
{
    public interface IScoreData
    {
        Int32 Id { get; set; }
        Decimal ReviewScore { get; set; }
    }
}
