﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.Common.Models
{
    public interface IProductData
    {
        Int32 Id { get; set; }
        String Name { get; set; }
    }
}
