﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.DAL.Queries
{
    internal static class ScoreQueries
    {
        public static String GET_AVG_RETING_BY_ID =
@"SELECT ProductId AS Id
       , AVG(Rating) AS ReviewScore
    FROM [Production].[ProductReview] 
   WHERE ProductID = @id
GROUP BY ProductID";

        public static String GET_AVG_RETING_BY_IDS =
@"  SELECT ProductId AS Id
         , AVG(Rating) AS ReviewScore
      FROM [Production].[ProductReview] pr
INNER JOIN @table AS t ON t.[Value] = pr.ProductID
  GROUP BY ProductID";

        //TODO: Need find a way to use full-text index.
        public static String GET_RATING_BY_ID_WITH_LIKE =
 @" SELECT ProductID AS Id
	     , SUM(t.Weight) AS ReviewScore
      FROM Production.ProductReview pr
CROSS JOIN [Production].[ScoreKeywords] t 
     WHERE pr.Comments LIKE CONCAT('%',t.Keyword,'%') AND ProductID = @id
  GROUP BY pr.ProductID";

        //TODO: Need find a way to use full-text index.
        public static String GET_RATING_BY_IDS_WITH_LIKE =
 @" SELECT ProductID AS Id
	     , SUM(t.Weight) AS ReviewScore
      FROM Production.ProductReview pr
INNER JOIN @table ids ON ids.Value = pr.ProductID
CROSS JOIN [Production].[ScoreKeywords] t 
     WHERE pr.Comments LIKE CONCAT('%',t.Keyword,'%')
  GROUP BY pr.ProductID";
        
        public const String GET_PREPARED_RATING_BY_ID =
@"SELECT ProductId AS Id
       , Rating AS ReviewScore 
    FROM [Production].[PreparedProductReviewReting] 
   WHERE ProductId = @id";

        public const String GET_PREPARED_RATING_BY_IDS =
@"  SELECT ProductId AS Id
         , pr.Rating AS ReviewScore
      FROM [Production].[PreparedProductReviewReting]  pr
INNER JOIN @table AS t ON t.[Value] = pr.ProductID";


    }
}
