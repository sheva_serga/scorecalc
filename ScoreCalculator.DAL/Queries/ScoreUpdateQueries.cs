﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.DAL.Queries
{
    internal sealed class ScoreUpdateQueries
    {
        public const String UPDATE_SCORE =
 @";WITH CTE AS (
    SELECT ProductID 
	     , SUM(t.Weight) as Rating
      FROM Production.ProductReview pr
CROSS JOIN [Production].[ScoreKeywords] t 
     WHERE pr.Comments LIKE CONCAT('%',t.Keyword,'%')
  GROUP BY pr.ProductID 
)
MERGE [Production].[PreparedProductReviewReting] AS target 
USING CTE AS source ON target.ProductID = source.ProductID
WHEN MATCHED AND (source.Rating != target.Rating) THEN 
	UPDATE SET Rating = source.Rating  
WHEN NOT MATCHED BY TARGET THEN  
    INSERT (ProductId , Rating) 
		VALUES (source.ProductId, source.Rating)
WHEN NOT MATCHED BY SOURCE THEN  
	DELETE;";

    }
}
