﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.DAL.Queries
{
    public static class ProductQueries
    {
        public const String GET_PRODUCT =
@"SELECT [ProductID] AS [Id]
       , [Name] 
    FROM [Production].[Product] 
   WHERE ProductID = @id";

        public const String GET_PRODUCT_PAGE = @"
DECLARE @offset INT = @pageSize * (@pageNumber - 1);

  SELECT [ProductID] AS [Id]
       , [Name] 
    FROM [Production].[Product] 
ORDER BY ProductID OFFSET @offset ROWS FETCH NEXT @pageSize ROWS ONLY;";
    }
}
