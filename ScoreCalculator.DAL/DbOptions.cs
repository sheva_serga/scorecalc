﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.DAL
{
    public class DbOptions
    {
        public String ConnectionString { get; set; }
    }
}
