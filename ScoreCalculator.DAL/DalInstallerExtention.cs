﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ScoreCalculator.Common.DAL;
using ScoreCalculator.DAL.Services;
using ScoreCalculator.DAL.Services.Score;
using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

[assembly: InternalsVisibleTo("ScoreCalculator.Banchmarks")]

namespace ScoreCalculator.DAL
{
    public static class DalInstallerExtention
    {
        public static IServiceCollection AddDalLayer(this IServiceCollection services, IConfiguration confuguration)
        {
            services.Configure<DbOptions>((o) => o.ConnectionString = confuguration["ConnectionString"]);

            services.AddSingleton<IDbStoreService, DbStoreService>();
            services.AddSingleton<IDbProductService, DbProductService>();
            services.AddSingleton<IDbScoreService, DbPreparedKeywordScoreService>();
            services.AddSingleton<IDbScoreUpdateService, DbPreparedKeywordScoreService>();

            return services;
        }
    }
}
