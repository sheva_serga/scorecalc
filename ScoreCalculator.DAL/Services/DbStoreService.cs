﻿using Dapper;
using Microsoft.Extensions.Options;
using ScoreCalculator.Common.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ScoreCalculator.DAL.Services
{
    internal class DbStoreService : IDbStoreService
    {
        private readonly String _connectioString;

        public DbStoreService(IOptions<DbOptions> options)
        {
            _connectioString = options.Value.ConnectionString;
        }

        public DbStoreService(String connectionString)
        {
            _connectioString = connectionString;
        }

        private SqlConnection CreateConnection() => new SqlConnection(_connectioString);

        private async Task<Int32> ExecuteAsync(String query, Object parameters)
        {
            using (IDbConnection connection = CreateConnection())
            {
                return await connection.ExecuteAsync(query, parameters);
            }
        }

        private async Task<T> SelectAsync<T>(String query, Object parameters)
        {
            using (IDbConnection connection = CreateConnection())
            {
                return (await connection.QueryAsync<T>(query, parameters)).FirstOrDefault();
            }
        }

        private async Task<List<TReturn>> SelectListAsync<T, TReturn>(String query, Object parameters) where T : TReturn
        {
            using (IDbConnection connection = CreateConnection())
            {
                return (await connection.QueryAsync<T>(query, parameters)).Cast<TReturn>().ToList();
            }
        }

        async Task<Int32> IDbStoreService.ExecuteAsync(String query, Object parameters) => await ExecuteAsync(query, parameters);
        async Task<T> IDbStoreService.SelectAsync<T>(String query, Object parameters) => await SelectAsync<T>(query, parameters);
        async Task<List<TResult>> IDbStoreService.SelectListAsync<T, TResult>(String query, Object parameters) => await SelectListAsync<T, TResult>(query, parameters);
    }
}
