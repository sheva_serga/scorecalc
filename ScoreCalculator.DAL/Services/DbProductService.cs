﻿using ScoreCalculator.Common.DAL;
using ScoreCalculator.Common.Models;
using ScoreCalculator.DAL.DTO;
using ScoreCalculator.DAL.Queries;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ScoreCalculator.DAL.Services
{
    internal class DbProductService : IDbProductService
    {
        private readonly IDbStoreService _storeService;

        public DbProductService(IDbStoreService storeService)
        {
            _storeService = storeService;
        }

        async Task<IProductData> IDbProductService.GetProductAsync(Int32 productId) =>
            await _storeService.SelectAsync<ProductDataDto>(ProductQueries.GET_PRODUCT, new { id = productId });

        async Task<List<IProductData>> IDbProductService.GetProductsByPageAsync(Int32 pageNumber, Int32 perPage) =>
            await _storeService.SelectListAsync<ProductDataDto, IProductData>(ProductQueries.GET_PRODUCT_PAGE, new { pageNumber, pageSize = perPage });
    }
}
