﻿using Dapper;
using ScoreCalculator.Common.DAL;
using ScoreCalculator.Common.Models;
using ScoreCalculator.DAL.DTO;
using ScoreCalculator.DAL.Queries;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCalculator.DAL.Services.Score
{
    internal class DbPreparedKeywordScoreService : IDbScoreUpdateService, IDbScoreService
    {
        private readonly IDbStoreService _storeService;

        public DbPreparedKeywordScoreService(IDbStoreService storeService)
        {
            _storeService = storeService;
        }

        void IDbScoreUpdateService.GenerateProductsRating() =>
            _storeService.ExecuteAsync(ScoreUpdateQueries.UPDATE_SCORE)
                .ConfigureAwait(false)
                .GetAwaiter();

        async Task<IScoreData> IDbScoreService.GetScoreDataAsync(Int32 productId) =>
            await _storeService.SelectAsync<ScoreDataDto>(ScoreQueries.GET_PREPARED_RATING_BY_ID, new { id = productId });

        async Task<List<IScoreData>> IDbScoreService.GetScoreDataAsync(IEnumerable<Int32> productIds)
        {
            DataTable idList = new DataTable();
            idList.Columns.Add("Value", typeof(Int32));

            foreach (Int32 item in productIds)
                idList.Rows.Add(item);

            return await _storeService.SelectListAsync<ScoreDataDto, IScoreData>(
                  ScoreQueries.GET_PREPARED_RATING_BY_IDS
                , new { table = idList.AsTableValuedParameter("dbo.IntArray") }
            );
        }
    }
}
