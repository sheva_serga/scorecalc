﻿using ScoreCalculator.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.DAL.DTO
{
    internal class ScoreDataDto : IScoreData
    {
        public Int32 Id { get; set; }
        public Decimal ReviewScore { get; set; }
    }
}
