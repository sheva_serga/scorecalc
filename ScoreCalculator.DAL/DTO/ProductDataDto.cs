﻿using ScoreCalculator.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ScoreCalculator.DAL.DTO
{
    internal class ProductDataDto : IProductData
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
    }
}
