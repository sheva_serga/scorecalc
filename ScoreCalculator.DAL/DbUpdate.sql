﻿CREATE TYPE dbo.IntArray AS TABLE(
	[Value] INT
);

-- Don't need to create index
CREATE TABLE [Production].[ScoreKeywords] (
	[Keyword] NVARCHAR(50), 	
	[Weight] DECIMAL
);

INSERT INTO [Production].[ScoreKeywords] VALUES 
	  ('terrible', -1)
	, ('bad', -1)
	, ('high-quality', 1)
	, ('very positive',1)
	, ('a blast', 1);

CREATE TABLE [Production].[PreparedProductReviewReting] (
	  ProductId INT CONSTRAINT [PK_PreparedProductReviewReting_ProductId] PRIMARY KEY
	, Rating DECIMAL
);
