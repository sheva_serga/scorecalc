﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ScoreCalculator.BLL.Services;

namespace ScoreCalculator.WEB.Controllers
{
    [Route(""), Produces("application/json")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService) => 
            _productService = productService;

        [Route("product/{productId}")]
        public async Task<ActionResult> GetProduct(Int32 productId)
        {
            if (productId <= 0) return BadRequest("Incorrect product id specified");

            return Json(await _productService.GetProductAsync(productId));
        }
            
        [Route("products")]
        public async Task<ActionResult> GetProducts(Int32 page, Int32 perPage = 10)
        {
            if (page <= 0) return BadRequest("Incorrect page specified");

            return Json(await _productService.GetProductModelsByPageAsync(page, perPage));
        }
        
    }
}
